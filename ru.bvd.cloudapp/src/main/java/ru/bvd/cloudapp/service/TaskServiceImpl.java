package ru.bvd.cloudapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bvd.cloudapp.api.TaskService;
import ru.bvd.cloudapp.api.dto.task.TaskDto;
import ru.bvd.cloudapp.db.StatusDao;
import ru.bvd.cloudapp.db.TaskDao;
import ru.bvd.cloudapp.db.entity.Task;
import ru.bvd.cloudapp.db.entity.Task.TaskStatus;
import ru.bvd.cloudapp.db.exception.NoDataFoundException;
import ru.bvd.cloudapp.db.exception.TooManyRowsException;
import ru.bvd.cloudapp.service.exception.ManyTasksFoundException;
import ru.bvd.cloudapp.service.exception.TaskNotFoundException;

import java.time.LocalDateTime;
import java.util.UUID;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

@Service
public class TaskServiceImpl implements TaskService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final TaskDao taskDao;
    private final StatusDao statusDao;

    private final TaskServiceAsync taskServiceAsync;

    public TaskServiceImpl(TaskDao taskDao, StatusDao statusDao, TaskServiceAsync taskServiceAsync) {
        this.taskDao = taskDao;
        this.statusDao = statusDao;
        this.taskServiceAsync = taskServiceAsync;
    }

    @Override
    @Transactional
    public TaskDto addTask() {
        log.info("call add task");

        Task task = taskDao.save(
                new Task(null, UUID.randomUUID(), statusDao.findStatusByCode(TaskStatus.CREATED), LocalDateTime.now())
        );
        log.info(task.toString());


        log.debug("updateTaskAsync will be call");
        taskServiceAsync.updateTask(task);

        return new TaskDto().withGuid(task.getUuid().toString())
                .withStatus(task.getStatus().getCode())
                .withTimestamp(ISO_LOCAL_DATE_TIME.format(task.getTimestamp()));

    }

    @Override
    public TaskDto getTaskByGuid(String guid) {
        UUID uuid = UUID.fromString(guid);

        Task task;
        try {
            task = taskDao.findTaskByUuid(uuid);
        } catch (TooManyRowsException e) {
            throw new ManyTasksFoundException(e);
        } catch (NoDataFoundException e) {
            throw new TaskNotFoundException(e);
        }

        return new TaskDto().withGuid(task.getUuid().toString())
                .withStatus(task.getStatus().getCode())
                .withTimestamp(ISO_LOCAL_DATE_TIME.format(task.getTimestamp()));
    }

    @Component
    public static class TaskServiceAsync {
        private final Logger log = LoggerFactory.getLogger( getClass() );

        private final TaskDao taskDao;
        private final StatusDao statusDao;

        public TaskServiceAsync(TaskDao taskDao, StatusDao statusDao) {
            this.taskDao = taskDao;
            this.statusDao = statusDao;
        }

        @Async
        public void updateTask(Task task) {
            log.debug("updateTask has called async");
            taskDao.update(
                    new Task(task.getId(), task.getUuid(), statusDao.findStatusByCode(TaskStatus.RUNNING), LocalDateTime.now())
            );
        }
    }
}
