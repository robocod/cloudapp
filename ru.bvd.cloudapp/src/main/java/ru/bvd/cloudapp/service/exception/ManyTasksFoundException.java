package ru.bvd.cloudapp.service.exception;

public class ManyTasksFoundException extends RuntimeException {
    public ManyTasksFoundException() {

    }

    public ManyTasksFoundException(String message) {
        super(message);
    }

    public ManyTasksFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ManyTasksFoundException (Throwable cause) {
        super(cause);
    }
}
