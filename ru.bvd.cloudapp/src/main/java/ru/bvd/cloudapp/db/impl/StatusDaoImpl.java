package ru.bvd.cloudapp.db.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.bvd.cloudapp.db.StatusDao;
import ru.bvd.cloudapp.db.entity.Status;
import ru.bvd.cloudapp.db.entity.Task;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

@Repository
public class StatusDaoImpl implements StatusDao {
    private final JdbcTemplate jdbcTemplate;

    public StatusDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Status findStatusByCode(Task.TaskStatus code) {
        return jdbcTemplate.queryForObject("SELECT * FROM t_status WHERE c_code=?", new StatusRowMapper(), code.getCode());
    }

    @Override
    public Status findStatusById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM t_status WHERE id=?", new StatusRowMapper(), id);
    }

    @Override
    public Optional<Status> get(int id) {
        return Optional.empty();
    }

    @Override
    public Collection<Status> getAll() {
        return null;
    }

    @Override
    public Status save(Status status) {
        return null;
    }

    @Override
    public void update(Status status) {

    }

    @Override
    public void delete(Status status) {

    }

    public static class StatusRowMapper implements RowMapper<Status> {
        @Override
        public Status mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Status(
                    rs.getLong("id"),
                    rs.getString("c_code"),
                    rs.getString("c_name")
            );
        }
    }

}
