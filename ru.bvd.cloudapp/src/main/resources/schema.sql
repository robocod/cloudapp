CREATE TABLE t_status (
   id IDENTITY,
   c_code VARCHAR(255) NOT NULL,
   c_name VARCHAR(255) NOT NULL,
   PRIMARY KEY(id),
   CONSTRAINT  uc_code UNIQUE(c_code)
);

CREATE TABLE t_task (
    id IDENTITY,
    c_uuid UUID NOT NULL,
    c_status long NOT NULL,
    c_timestamp TIMESTAMP NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(c_status) REFERENCES t_status(id)
)