package ru.bvd.cloudapp.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.bvd.cloudapp.api.TaskService;
import ru.bvd.cloudapp.api.dto.task.TaskDto;

import java.time.LocalDateTime;
import java.util.UUID;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.bvd.cloudapp.db.entity.Task.TaskStatus.CREATED;

@RunWith(SpringRunner.class)
@WebMvcTest(TaskRestController.class)
public class TaskRestControllerTest {
    private static final LocalDateTime TIMESTAMP = LocalDateTime.of(1995,3,8,14,15,32);
    private static final ObjectReader objectReader = new ObjectMapper().readerFor(TaskDto.class);

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TaskService taskService;

    @Test
    public void newTaskAdded() throws Exception {
        TaskDto expectedTaskDto = new TaskDto().withGuid("1")
                .withStatus(CREATED.getCode())
                .withTimestamp(ISO_LOCAL_DATE_TIME.format(TIMESTAMP));

        given(taskService.addTask()).willReturn(expectedTaskDto);

        mvc.perform(post("/task"))
                .andExpect(status().isAccepted())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(mvcResult -> {
                    String contentAsString = mvcResult.getResponse().getContentAsString();
                    TaskDto actualTaskDto = objectReader.readValue(contentAsString);
                    assertEquals(expectedTaskDto, actualTaskDto);
                });
    }

    @Test
    public void existTaskByIdGiven() throws Exception {
        String guid = UUID.randomUUID().toString();
        TaskDto expectedTaskDto = new TaskDto().withGuid(guid)
                .withStatus(CREATED.getCode())
                .withTimestamp(ISO_LOCAL_DATE_TIME.format(TIMESTAMP));

        given(taskService.getTaskByGuid(guid)).willReturn(expectedTaskDto);

        mvc.perform(get("/task/" + guid))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(mvcResult -> {
                    String contentAsString = mvcResult.getResponse().getContentAsString();
                    TaskDto actualTaskDto = objectReader.readValue(contentAsString);
                    assertEquals(expectedTaskDto, actualTaskDto);
                });
    }
}